package com.getbridge.homework.web.it

import com.fasterxml.jackson.module.kotlin.readValue
import com.getbridge.homework.data.repository.DiscussionRepository
import com.getbridge.homework.web.domain.DiscussionView
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus

class GetAllDiscussionsTest : IntegrationTestBase() {

    @Autowired
    lateinit var discussionRepository: DiscussionRepository

    @Test
    fun `test get all discussions should return all discussions`() {

        val actual = performGet("/discussions")

        assertEquals(HttpStatus.OK.value(), actual.response.status)
        val discussions: List<DiscussionView> = objectMapper.readValue(actual.response.contentAsString)
        assertEquals(discussionRepository.findAll().size, discussions.size)
    }

}