package com.getbridge.homework.data.dao

import com.getbridge.homework.data.entity.UserEntity
import com.getbridge.homework.data.repository.UserRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension
import java.util.Optional

private const val ID = 1L
private val USER = UserEntity(ID, "Steve")

@ExtendWith(MockitoExtension::class)
internal class UserDaoTest {

    @Mock
    lateinit var userRepository: UserRepository

    @InjectMocks
    lateinit var underTest: UserDao

    @Test
    fun `test find user by id should return user when called with id that exist`() {
        val expected = Optional.of(USER)
        `when`(userRepository.findById(ID)).thenReturn(expected)

        val actual = underTest.findById(ID)

        assertEquals(expected, actual)
    }
}