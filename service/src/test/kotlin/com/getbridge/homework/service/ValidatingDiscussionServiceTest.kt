package com.getbridge.homework.service

import com.getbridge.homework.data.dao.LocationDao
import com.getbridge.homework.data.dao.UserDao
import com.getbridge.homework.data.entity.LocationEntity
import com.getbridge.homework.data.entity.UserEntity
import com.getbridge.homework.service.domain.Discussion
import com.getbridge.homework.service.domain.EntityNotFoundException
import com.getbridge.homework.service.domain.Location
import com.getbridge.homework.service.domain.User
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension
import java.time.LocalDateTime
import java.time.Month
import java.util.Optional

private const val ID_ONE = 1L
private const val ID_TWO = 2L
private const val NAME = "name"
private const val TITLE = "title"
private const val DESCRIPTION = "description"

private val DATE = LocalDateTime.of(2022, Month.FEBRUARY, 4, 15, 0)

private val LOCATION = Location(ID_ONE, NAME)
private val LOCATION_ENTITY = LocationEntity(ID_ONE, NAME)

private val USER_ONE = User(ID_ONE, NAME)
private val USER_TWO = User(ID_TWO, NAME)

private val USER_ENTITY_ONE = UserEntity(ID_ONE, NAME)
private val USER_ENTITY_TWO = UserEntity(ID_TWO, NAME)

private val DISCUSSION = Discussion(
    id = ID_ONE,
    title = TITLE,
    description = DESCRIPTION,
    participants = listOf(USER_ONE, USER_TWO),
    location = LOCATION,
    closed = true,
    plannedDate = DATE,
    createdDate = DATE,
    lastModifiedDate = DATE
)

private const val EXPECTED_LOCATION_ERROR = "Location not found with id: 1"
private const val EXPECTED_USER_ONE_ERROR = "User not found with id: 1"
private const val EXPECTED_USER_TWO_ERROR = "User not found with id: 2"

@ExtendWith(MockitoExtension::class)
internal class ValidatingDiscussionServiceTest {

    @Mock
    lateinit var userDao: UserDao

    @Mock
    lateinit var locationDao: LocationDao

    @Mock
    lateinit var defaultDiscussionService: DiscussionService

    @InjectMocks
    lateinit var underTest: ValidatingDiscussionService


    @Test
    fun `test save discussion should save discussion when participants and location exist`() {
        `when`(locationDao.findById(ID_ONE)).thenReturn(Optional.of(LOCATION_ENTITY))
        `when`(userDao.findById(ID_ONE)).thenReturn(Optional.of(USER_ENTITY_ONE))
        `when`(userDao.findById(ID_TWO)).thenReturn(Optional.of(USER_ENTITY_TWO))
        `when`(defaultDiscussionService.saveDiscussion(DISCUSSION)).thenReturn(DISCUSSION)

        val actual = underTest.saveDiscussion(DISCUSSION)

        assertEquals(DISCUSSION, actual)
    }

    @Test
    fun `test save discussion should throw entity not found exception when location does not exist`() {
        `when`(locationDao.findById(ID_ONE)).thenReturn(Optional.empty())

        val actual = assertThrows(EntityNotFoundException::class.java) {
            underTest.saveDiscussion(DISCUSSION)
        }

        assertEquals(EXPECTED_LOCATION_ERROR, actual.message)
    }

    @Test
    fun `test save discussion should throw entity not found exception when the first participant`() {
        `when`(locationDao.findById(ID_ONE)).thenReturn(Optional.of(LOCATION_ENTITY))
        `when`(userDao.findById(ID_ONE)).thenReturn(Optional.empty())

        val actual = assertThrows(EntityNotFoundException::class.java) {
            underTest.saveDiscussion(DISCUSSION)
        }

        assertEquals(EXPECTED_USER_ONE_ERROR, actual.message)
    }

    @Test
    fun `test save discussion should throw entity not found exception when the second participant`() {
        `when`(locationDao.findById(ID_ONE)).thenReturn(Optional.of(LOCATION_ENTITY))
        `when`(userDao.findById(ID_ONE)).thenReturn(Optional.of(USER_ENTITY_ONE))
        `when`(userDao.findById(ID_TWO)).thenReturn(Optional.empty())

        val actual = assertThrows(EntityNotFoundException::class.java) {
            underTest.saveDiscussion(DISCUSSION)
        }

        assertEquals(EXPECTED_USER_TWO_ERROR, actual.message)
    }

    @Test
    fun `test update discussion should update discussion when participants and location exist`() {
        `when`(locationDao.findById(ID_ONE)).thenReturn(Optional.of(LOCATION_ENTITY))
        `when`(userDao.findById(ID_ONE)).thenReturn(Optional.of(USER_ENTITY_ONE))
        `when`(userDao.findById(ID_TWO)).thenReturn(Optional.of(USER_ENTITY_TWO))
        `when`(defaultDiscussionService.updateDiscussion(ID_ONE, DISCUSSION, ID_ONE)).thenReturn(DISCUSSION)

        val actual = underTest.updateDiscussion(ID_ONE, DISCUSSION, ID_ONE)

        assertEquals(DISCUSSION, actual)
    }

    @Test
    fun `test update discussion should throw entity not found exception when location does not exist`() {
        `when`(locationDao.findById(ID_ONE)).thenReturn(Optional.empty())

        val actual = assertThrows(EntityNotFoundException::class.java) {
            underTest.updateDiscussion(ID_ONE, DISCUSSION, ID_ONE)
        }

        assertEquals(EXPECTED_LOCATION_ERROR, actual.message)
    }

    @Test
    fun `test update discussion should throw entity not found exception when the first participant`() {
        `when`(locationDao.findById(ID_ONE)).thenReturn(Optional.of(LOCATION_ENTITY))
        `when`(userDao.findById(ID_ONE)).thenReturn(Optional.empty())

        val actual = assertThrows(EntityNotFoundException::class.java) {
            underTest.updateDiscussion(ID_ONE, DISCUSSION, ID_ONE)
        }

        assertEquals(EXPECTED_USER_ONE_ERROR, actual.message)
    }

    @Test
    fun `test update discussion should throw entity not found exception when the second participant`() {
        `when`(locationDao.findById(ID_ONE)).thenReturn(Optional.of(LOCATION_ENTITY))
        `when`(userDao.findById(ID_ONE)).thenReturn(Optional.of(USER_ENTITY_ONE))
        `when`(userDao.findById(ID_TWO)).thenReturn(Optional.empty())

        val actual = assertThrows(EntityNotFoundException::class.java) {
            underTest.updateDiscussion(ID_ONE, DISCUSSION, ID_ONE)
        }

        assertEquals(EXPECTED_USER_TWO_ERROR, actual.message)
    }
}