package com.getbridge.homework.data.dao

import com.getbridge.homework.data.repository.LocationRepository
import org.springframework.stereotype.Component

@Component
class LocationDao(
    private val locationRepository: LocationRepository
) {
    fun findById(id: Long) = locationRepository.findById(id)
}