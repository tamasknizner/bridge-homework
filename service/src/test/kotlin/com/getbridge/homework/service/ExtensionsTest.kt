package com.getbridge.homework.service

import com.getbridge.homework.data.entity.DiscussionEntity
import com.getbridge.homework.data.entity.LocationEntity
import com.getbridge.homework.data.entity.UserEntity
import com.getbridge.homework.service.domain.Discussion
import com.getbridge.homework.service.domain.Location
import com.getbridge.homework.service.domain.User
import com.getbridge.homework.service.domain.toDomain
import com.getbridge.homework.service.domain.toEntity
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.time.Month

private const val ID = 1L
private const val NAME = "name"
private const val TITLE = "title"
private const val DESCRIPTION = "description"

private val USER_ENTITY = UserEntity(ID, NAME)
private val USER = User(ID, NAME)

private val LOCATION_ENTITY = LocationEntity(ID, NAME)
private val LOCATION = Location(ID, NAME)

private val DATE = LocalDateTime.of(2022, Month.FEBRUARY, 4, 15, 0)
private val DISCUSSION_ENTITY = DiscussionEntity(
    id = ID,
    title = TITLE,
    description = DESCRIPTION,
    participants = listOf(USER_ENTITY),
    location = LOCATION_ENTITY,
    closed = true,
    plannedDate = DATE,
    createdDate = DATE,
    lastModifiedDate = DATE
)

private val DISCUSSION = Discussion(
    id = ID,
    title = TITLE,
    description = DESCRIPTION,
    participants = listOf(USER),
    location = LOCATION,
    closed = true,
    plannedDate = DATE,
    createdDate = DATE,
    lastModifiedDate = DATE
)

internal class ExtensionsTest {

    @Test
    fun `test user to entity should convert domain object to entity`() {

        val actual = USER.toEntity()

        assertEquals(USER_ENTITY, actual)
    }

    @Test
    fun `test user entity to domain should convert entity to domain object`() {

        val actual = USER_ENTITY.toDomain()

        assertEquals(USER, actual)
    }

    @Test
    fun `test location to entity should convert domain object to entity`() {

        val actual = LOCATION.toEntity()

        assertEquals(LOCATION_ENTITY, actual)
    }

    @Test
    fun `test location entity to domain should convert entity to domain object`() {

        val actual = LOCATION_ENTITY.toDomain()

        assertEquals(LOCATION, actual)
    }

    @Test
    fun `test discussion to entity should convert domain object to entity`() {

        val actual = DISCUSSION.toEntity()

        assertEquals(DISCUSSION_ENTITY, actual)
    }

    @Test
    fun `test discussion entity to domain should convert entity to domain object`() {

        val actual = DISCUSSION_ENTITY.toDomain()

        assertEquals(DISCUSSION, actual)
    }
}
