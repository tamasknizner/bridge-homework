package com.getbridge.homework.data.initializer

import com.getbridge.homework.data.entity.DiscussionEntity
import com.getbridge.homework.data.entity.LocationEntity
import com.getbridge.homework.data.entity.UserEntity
import com.getbridge.homework.data.repository.DiscussionRepository
import com.getbridge.homework.data.repository.LocationRepository
import com.getbridge.homework.data.repository.UserRepository
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.time.Month

@Component
class DatabaseInitializer(
    private val userRepository: UserRepository,
    private val locationRepository: LocationRepository,
    private val discussionRepository: DiscussionRepository,
    @Value("\${database.init:false}") private val initDatabase: Boolean
) : CommandLineRunner {

    override fun run(vararg args: String?) {
        if (shouldInitialize()) {
            insertUsers()
            insertLocations()
            insertDiscussions()
        }
    }

    private fun shouldInitialize() = initDatabase && userRepository.findAll().isEmpty()

    private fun insertUsers() {
        userRepository.saveAll(listOf(steve, joe, kathy))
    }

    private fun insertLocations() {
        locationRepository.saveAll(listOf(teams, zoom))
    }

    private fun insertDiscussions() {
        val steveAndKathyOnZoom = DiscussionEntity(
            title = "Steve and Kathy on Zoom",
            description = "Hi Kathy, please meet me on Zoom",
            participants = listOf(steve, kathy),
            location = teams,
            plannedDate = LocalDateTime.of(2022, Month.FEBRUARY, 4, 15, 0),
            closed = false
        )
        discussionRepository.save(steveAndKathyOnZoom)
    }

    companion object {
        val steve = UserEntity(1L, "Steve")
        val joe = UserEntity(2L, "Joe")
        val kathy = UserEntity(3L, "Kathy")
        val teams = LocationEntity(1L, "Teams")
        val zoom = LocationEntity(2L, "Zoom")
    }
}