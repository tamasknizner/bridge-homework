package com.getbridge.homework.web.it

import com.fasterxml.jackson.module.kotlin.readValue
import com.getbridge.homework.data.repository.DiscussionRepository
import com.getbridge.homework.web.domain.DiscussionView
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus

class SearchDiscussionsTest : IntegrationTestBase() {

    @Autowired
    lateinit var discussionRepository: DiscussionRepository

    @Test
    fun `test search closed discussions should return closed discussions when called`() {
        val actual = performGet("/discussions/search?closed=true")

        assertEquals(HttpStatus.OK.value(), actual.response.status)

        val discussions: List<DiscussionView> = parseResponse(actual.response.contentAsString)
        assertEquals(retrieveEntities(true).size, discussions.size)
    }

    @Test
    fun `test search not closed discussions should return not closed discussions when called`() {
        val actual = performGet("/discussions/search?closed=false")

        assertEquals(HttpStatus.OK.value(), actual.response.status)

        val discussions: List<DiscussionView> = parseResponse(actual.response.contentAsString)
        assertEquals(retrieveEntities(false).size, discussions.size)
    }

    private fun parseResponse(response: String): List<DiscussionView> = objectMapper.readValue(response)

    private fun retrieveEntities(closed: Boolean) = discussionRepository.findAllByClosedIs(closed)
}