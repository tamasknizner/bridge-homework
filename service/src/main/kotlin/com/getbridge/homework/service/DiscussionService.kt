package com.getbridge.homework.service

import com.getbridge.homework.service.domain.Discussion

interface DiscussionService {

    fun saveDiscussion(discussion: Discussion): Discussion

    fun updateDiscussion(discussionId: Long, discussion: Discussion, userId: Long): Discussion

    fun findDiscussionById(id: Long, userId: Long): Discussion

    fun deleteDiscussionById(id: Long, userId: Long)

    fun findAllDiscussions(): List<Discussion>

    fun closeDiscussion(id: Long, userId: Long)

    fun searchDiscussions(closed: Boolean): List<Discussion>
}