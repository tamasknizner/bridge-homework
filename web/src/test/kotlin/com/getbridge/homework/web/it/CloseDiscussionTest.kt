package com.getbridge.homework.web.it

import com.getbridge.homework.data.repository.DiscussionRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus

class CloseDiscussionTest : IntegrationTestBase() {

    @Autowired
    lateinit var discussionRepository: DiscussionRepository

    @Test
    fun `test close discussion should close discussion when called`() {
        val actual = performPost("/discussion/15/close")

        assertEquals(HttpStatus.OK.value(), actual.response.status)
        assertTrue(discussionRepository.getById(15).closed)
    }
}