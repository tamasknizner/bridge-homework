package com.getbridge.homework.web.it

import com.getbridge.homework.web.domain.DiscussionRequest
import net.javacrumbs.jsonunit.assertj.assertThatJson
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.http.HttpStatus
import java.time.LocalDateTime
import java.time.Month

private const val URI = "/discussion"

private val DATE = LocalDateTime.of(2022, Month.FEBRUARY, 20, 15, 0)

class CreateDiscussionTest : IntegrationTestBase() {

    @Test
    fun `test create discussion should create discussion when valid and exist`() {

        val request = DiscussionRequest(
            title = "new title",
            participants = listOf(1L, 2L),
            location = 1L,
            plannedDate = DATE,
            description = "new description"
        )

        val actual = performPost(URI, request)

        assertEquals(HttpStatus.OK.value(), actual.response.status)
        assertThatJson(actual.response.contentAsString).isEqualTo(expected("create-discussion"))
    }

    @ParameterizedTest
    @MethodSource("provideRequestsForBadRequestCases")
    fun `test create discussion should return bad request when request is invalid`(request: DiscussionRequest) {

        val actual = performPost(URI, request)

        assertEquals(HttpStatus.BAD_REQUEST.value(), actual.response.status)
    }

    companion object {
        @JvmStatic
        fun provideRequestsForBadRequestCases(): List<Arguments> {
            return listOf(
                Arguments.of(
                    DiscussionRequest(
                        title = "invalid location id",
                        participants = listOf(1L, 3L),
                        location = 70L,
                        plannedDate = DATE,
                        description = "description"
                    )
                ),
                Arguments.of(
                    DiscussionRequest(
                        title = "",
                        participants = listOf(1L, 3L),
                        location = 2L,
                        plannedDate = DATE,
                        description = "blank title"
                    )
                ),
                Arguments.of(
                    DiscussionRequest(
                        title = "blank description",
                        participants = listOf(1L, 3L),
                        location = 2L,
                        plannedDate = DATE,
                        description = ""
                    )
                ),
                Arguments.of(
                    DiscussionRequest(
                        title = "participant id does not exist",
                        participants = listOf(1L, 70L),
                        location = 2L,
                        plannedDate = DATE,
                        description = "description"
                    )
                ),
                Arguments.of(
                    DiscussionRequest(
                        title = "participant list count is not 2",
                        participants = listOf(1L),
                        location = 2L,
                        plannedDate = DATE,
                        description = "description"
                    )
                )
            )
        }
    }
}