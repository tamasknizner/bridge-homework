package com.getbridge.homework.service

import com.getbridge.homework.data.dao.DiscussionDao
import com.getbridge.homework.data.dao.LocationDao
import com.getbridge.homework.data.dao.UserDao
import com.getbridge.homework.data.entity.DiscussionEntity
import com.getbridge.homework.data.entity.LocationEntity
import com.getbridge.homework.data.entity.UserEntity
import com.getbridge.homework.service.domain.Discussion
import com.getbridge.homework.service.domain.DiscussionReadOnlyException
import com.getbridge.homework.service.domain.Location
import com.getbridge.homework.service.domain.User
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.doNothing
import org.mockito.Mockito.verify
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import java.time.LocalDateTime
import java.time.Month
import java.util.Optional

private const val ID_ONE = 1L
private const val ID_TWO = 2L
private const val NAME = "name"
private const val TITLE = "title"
private const val UPDATED_TITLE = "updated title"
private const val DESCRIPTION = "description"
private const val UPDATED_DESCRIPTION = "updated description"

private val USER_ENTITY = UserEntity(ID_ONE, NAME)
private val USER = User(ID_ONE, NAME)

private val UPDATED_USER_ENTITY = UserEntity(ID_TWO, NAME)
private val UPDATED_USER = User(ID_TWO, NAME)

private val LOCATION_ENTITY = LocationEntity(ID_ONE, NAME)
private val LOCATION = Location(ID_ONE, NAME)

private val UPDATED_LOCATION_ENTITY = LocationEntity(ID_TWO, NAME)
private val UPDATED_LOCATION = Location(ID_TWO, NAME)

private val DATE = LocalDateTime.of(2022, Month.FEBRUARY, 4, 15, 0)
private val UPDATED_DATE = LocalDateTime.of(2022, Month.FEBRUARY, 5, 15, 0)
private val DISCUSSION_ENTITY = DiscussionEntity(
    id = ID_ONE,
    title = TITLE,
    description = DESCRIPTION,
    participants = listOf(USER_ENTITY),
    location = LOCATION_ENTITY,
    closed = false,
    plannedDate = DATE,
    createdDate = DATE,
    lastModifiedDate = DATE
)

private val CLOSED_DISCUSSION_ENTITY = DiscussionEntity(
    id = ID_ONE,
    title = TITLE,
    description = DESCRIPTION,
    participants = listOf(USER_ENTITY),
    location = LOCATION_ENTITY,
    closed = true,
    plannedDate = DATE,
    createdDate = DATE,
    lastModifiedDate = DATE
)

private val UPDATED_ENTITY = DiscussionEntity(
    id = ID_ONE,
    title = UPDATED_TITLE,
    description = UPDATED_DESCRIPTION,
    participants = listOf(UPDATED_USER_ENTITY),
    location = UPDATED_LOCATION_ENTITY,
    closed = false,
    plannedDate = UPDATED_DATE,
    createdDate = DATE,
    lastModifiedDate = DATE
)

private val DISCUSSION = Discussion(
    id = ID_ONE,
    title = TITLE,
    description = DESCRIPTION,
    participants = listOf(USER),
    location = LOCATION,
    closed = false,
    plannedDate = DATE,
    createdDate = DATE,
    lastModifiedDate = DATE
)

private val UPDATED_DISCUSSION = Discussion(
    id = ID_ONE,
    title = UPDATED_TITLE,
    description = UPDATED_DESCRIPTION,
    participants = listOf(UPDATED_USER),
    location = UPDATED_LOCATION,
    closed = false,
    plannedDate = UPDATED_DATE,
    createdDate = DATE,
    lastModifiedDate = DATE
)

private const val DISCUSSION_CLOSED_MESSAGE = "Discussion with id: 1 is closed and cannot be modified."

@ExtendWith(MockitoExtension::class)
internal class DefaultDiscussionServiceTest {

    @Mock
    lateinit var discussionDao: DiscussionDao

    @Mock
    lateinit var userDao: UserDao

    @Mock
    lateinit var locationDao: LocationDao

    @InjectMocks
    lateinit var underTest: DefaultDiscussionService

    @Test
    fun `test save discussion should saved discussion and return the saved object`() {
        `when`(userDao.findById(ID_ONE)).thenReturn(Optional.of(USER_ENTITY))
        `when`(locationDao.findById(ID_ONE)).thenReturn(Optional.of(LOCATION_ENTITY))
        `when`(discussionDao.save(DISCUSSION_ENTITY)).thenReturn(DISCUSSION_ENTITY)

        val actual = underTest.saveDiscussion(DISCUSSION)

        assertEquals(DISCUSSION, actual)
    }

    @Test
    fun `test find discussion by id should return the discussion when found`() {
        `when`(discussionDao.findById(ID_ONE)).thenReturn(Optional.of(DISCUSSION_ENTITY))

        val actual = underTest.findDiscussionById(ID_ONE, ID_ONE)

        assertEquals(DISCUSSION, actual)
    }

    @Test
    fun `test find discussion by id should throw response status exception when discussion is not found`() {
        `when`(discussionDao.findById(ID_ONE)).thenReturn(Optional.empty())

        val actual = assertThrows(ResponseStatusException::class.java) {
            underTest.findDiscussionById(ID_ONE, ID_ONE)
        }

        assertEquals(HttpStatus.NOT_FOUND, actual.status)
    }

    @Test
    fun `test find discussion by id should throw response status exception when user is not owner`() {
        `when`(discussionDao.findById(ID_ONE)).thenReturn(Optional.of(DISCUSSION_ENTITY))

        val actual = assertThrows(ResponseStatusException::class.java) {
            underTest.findDiscussionById(ID_ONE, 3L)
        }

        assertEquals(HttpStatus.FORBIDDEN, actual.status)
    }

    @Test
    fun `test update discussion should update discussion when exist`() {
        `when`(discussionDao.findById(ID_ONE)).thenReturn(Optional.of(DISCUSSION_ENTITY))
        `when`(discussionDao.save(UPDATED_ENTITY)).thenReturn(UPDATED_ENTITY)

        val actual = underTest.updateDiscussion(ID_ONE, UPDATED_DISCUSSION, ID_ONE)

        assertEquals(UPDATED_DISCUSSION, actual)
        verify(discussionDao).findById(ID_ONE)
        verify(discussionDao).save(UPDATED_ENTITY)
    }

    @Test
    fun `test update discussion should throw response status exception when discussion is not found`() {
        `when`(discussionDao.findById(ID_ONE)).thenReturn(Optional.empty())

        val actual = assertThrows(ResponseStatusException::class.java) {
            underTest.updateDiscussion(ID_ONE, UPDATED_DISCUSSION, ID_ONE)
        }

        assertEquals(HttpStatus.NOT_FOUND, actual.status)
        verify(discussionDao).findById(ID_ONE)
    }

    @Test
    fun `test update discussion should throw exception when discussion is closed`() {
        `when`(discussionDao.findById(ID_ONE)).thenReturn(Optional.of(CLOSED_DISCUSSION_ENTITY))

        val actual = assertThrows(DiscussionReadOnlyException::class.java) {
            underTest.updateDiscussion(ID_ONE, UPDATED_DISCUSSION, ID_ONE)
        }

        assertEquals(DISCUSSION_CLOSED_MESSAGE, actual.message)
        verify(discussionDao).findById(ID_ONE)
    }

    @Test
    fun `test delete discussion by id should call dao to delete discussion when called`() {
        `when`(discussionDao.findById(ID_ONE)).thenReturn(Optional.of(DISCUSSION_ENTITY))
        doNothing().`when`(discussionDao).deleteById(ID_ONE)

        underTest.deleteDiscussionById(ID_ONE, ID_ONE)

        verify(discussionDao).findById(ID_ONE)
        verify(discussionDao).deleteById(ID_ONE)
    }

    @Test
    fun `test delete discussion by id should throw response status exception when discussion is not found`() {
        `when`(discussionDao.findById(ID_ONE)).thenReturn(Optional.empty())

        val actual = assertThrows(ResponseStatusException::class.java) {
            underTest.deleteDiscussionById(ID_ONE, ID_ONE)
        }

        assertEquals(HttpStatus.NOT_FOUND, actual.status)
        verify(discussionDao).findById(ID_ONE)
    }

    @Test
    fun `test find all discussion should return all discussion when called`() {
        `when`(discussionDao.findAll()).thenReturn(listOf(DISCUSSION_ENTITY))

        val actual = underTest.findAllDiscussions()

        assertEquals(listOf(DISCUSSION), actual)
    }

    @Test
    fun `test close discussion should save closed discussion when called`() {
        `when`(discussionDao.findById(ID_ONE)).thenReturn(Optional.of(DISCUSSION_ENTITY))
        `when`(discussionDao.save(CLOSED_DISCUSSION_ENTITY)).thenReturn(CLOSED_DISCUSSION_ENTITY)

        underTest.closeDiscussion(ID_ONE, ID_ONE)

        verify(discussionDao).findById(ID_ONE)
        verify(discussionDao).save(CLOSED_DISCUSSION_ENTITY)
    }

    @Test
    fun `test search discussions should return discussions with the appropriate closed state when called`() {
        `when`(discussionDao.findAllByClosed(false)).thenReturn(listOf(DISCUSSION_ENTITY))

        val actual = underTest.searchDiscussions(false)

        assertEquals(listOf(DISCUSSION), actual)
    }
}