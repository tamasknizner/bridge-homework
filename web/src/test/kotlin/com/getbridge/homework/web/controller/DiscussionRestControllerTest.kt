package com.getbridge.homework.web.controller

import com.getbridge.homework.service.DefaultDiscussionService
import com.getbridge.homework.service.domain.Discussion
import com.getbridge.homework.service.domain.Location
import com.getbridge.homework.service.domain.User
import com.getbridge.homework.web.domain.DiscussionRequest
import com.getbridge.homework.web.domain.DiscussionView
import com.getbridge.homework.web.domain.LocationView
import com.getbridge.homework.web.domain.UserView
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.doNothing
import org.mockito.Mockito.verify
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.http.ResponseEntity
import java.time.LocalDateTime
import java.time.Month

private const val ID = 1L
private const val NAME = "name"
private const val TITLE = "title"
private const val DESCRIPTION = "description"
private val DATE = LocalDateTime.of(2022, Month.FEBRUARY, 4, 15, 0)

private val USER = User(ID)
private val NAMED_USER = User(ID, NAME)
private val USER_VIEW = UserView(ID, NAME)

private val LOCATION = Location(ID)
private val NAMED_LOCATION = Location(ID, NAME)
private val LOCATION_VIEW = LocationView(ID, NAME)

private val DISCUSSION_REQUEST = DiscussionRequest(
    title = TITLE,
    participants = listOf(ID),
    location = ID,
    plannedDate = DATE,
    description = DESCRIPTION
)

private val DISCUSSION = Discussion(
    title = TITLE,
    participants = listOf(USER),
    location = LOCATION,
    plannedDate = DATE,
    description = DESCRIPTION
)

private val SAVED_DISCUSSION = Discussion(
    id = ID,
    title = TITLE,
    participants = listOf(NAMED_USER),
    location = NAMED_LOCATION,
    plannedDate = DATE,
    description = DESCRIPTION,
    closed = false,
    createdDate = DATE,
    lastModifiedDate = DATE
)

private val DISCUSSION_VIEW = DiscussionView(
    id = ID,
    title = TITLE,
    participants = listOf(USER_VIEW),
    location = LOCATION_VIEW,
    plannedDate = DATE,
    description = DESCRIPTION,
    closed = false,
    createdDate = DATE,
    lastModifiedDate = DATE
)

@ExtendWith(MockitoExtension::class)
internal class DiscussionRestControllerTest {

    @Mock
    lateinit var discussionService: DefaultDiscussionService

    @InjectMocks
    lateinit var underTest: DiscussionRestController

    @Test
    fun `test save discussion should return saved discussion when called`() {
        `when`(discussionService.saveDiscussion(DISCUSSION)).thenReturn(SAVED_DISCUSSION)

        val actual = underTest.saveDiscussion(DISCUSSION_REQUEST)

        assertEquals(ResponseEntity.ok(DISCUSSION_VIEW), actual)
    }

    @Test
    fun `test update discussion should return updated discussion when called`() {
        `when`(discussionService.updateDiscussion(ID, DISCUSSION, ID)).thenReturn(SAVED_DISCUSSION)

        val actual = underTest.updateDiscussion(ID, DISCUSSION_REQUEST, ID)

        assertEquals(ResponseEntity.ok(DISCUSSION_VIEW), actual)
    }

    @Test
    fun `test get discussion by id should return discussion when called`() {
        `when`(discussionService.findDiscussionById(ID, ID)).thenReturn(SAVED_DISCUSSION)

        val actual = underTest.getDiscussionById(ID, ID)

        assertEquals(ResponseEntity.ok(DISCUSSION_VIEW), actual)
    }

    @Test
    fun `test get all discussions should return all discussion when called`() {
        `when`(discussionService.findAllDiscussions()).thenReturn(listOf(SAVED_DISCUSSION))

        val actual = underTest.getAllDiscussions()

        assertEquals(ResponseEntity.ok(listOf(DISCUSSION_VIEW)), actual)
    }

    @Test
    fun `test delete discussion by id should delete discussion and return http status ok when called`() {
        val expected: ResponseEntity<Unit> = ResponseEntity.ok().build()
        doNothing().`when`(discussionService).deleteDiscussionById(ID, ID)

        val actual = underTest.deleteDiscussionById(ID, ID)

        assertEquals(expected, actual)
        verify(discussionService).deleteDiscussionById(ID, ID)
    }

    @Test
    fun `test close discussion by id should close discussion and return http status ok when called`() {
        val expected: ResponseEntity<Unit> = ResponseEntity.ok().build()
        doNothing().`when`(discussionService).closeDiscussion(ID, ID)

        val actual = underTest.closeDiscussion(ID, ID)

        assertEquals(expected, actual)
        verify(discussionService).closeDiscussion(ID, ID)
    }

    @Test
    fun `test search discussions should return discussions with the appropriate closed state when called`() {
        `when`(discussionService.searchDiscussions(false)).thenReturn(listOf(SAVED_DISCUSSION))

        val actual = underTest.searchDiscussions(false)

        assertEquals(ResponseEntity.ok(listOf(DISCUSSION_VIEW)), actual)
    }
}