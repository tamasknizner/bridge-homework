package com.getbridge.homework.service.domain

import com.getbridge.homework.data.entity.DiscussionEntity
import com.getbridge.homework.data.entity.LocationEntity
import com.getbridge.homework.data.entity.UserEntity
import java.time.LocalDateTime

fun User.toEntity() = UserEntity(id, name)

fun UserEntity.toDomain() = User(id ?: 0L, name)

fun Location.toEntity() = LocationEntity(id, name)

fun LocationEntity.toDomain() = Location(id ?: 0L, name)

fun Discussion.toEntity() = DiscussionEntity(
    id = id,
    title = title,
    description = description,
    participants = participants.map { user -> user.toEntity() },
    location = location.toEntity(),
    closed = closed,
    plannedDate = plannedDate,
    createdDate = createdDate,
    lastModifiedDate = lastModifiedDate
)

fun Discussion.toClosed() = Discussion(
    id = id,
    title = title,
    description = description,
    participants = participants,
    location = location,
    closed = true,
    plannedDate = plannedDate,
    createdDate = createdDate,
    lastModifiedDate = lastModifiedDate
)

fun DiscussionEntity.toDomain() = Discussion(
    id = id ?: 0L,
    title = title,
    description = description,
    participants = participants.map { user -> user.toDomain() },
    location = location.toDomain(),
    closed = closed,
    plannedDate = plannedDate,
    createdDate = createdDate ?: LocalDateTime.now(),
    lastModifiedDate = lastModifiedDate ?: LocalDateTime.now()
)