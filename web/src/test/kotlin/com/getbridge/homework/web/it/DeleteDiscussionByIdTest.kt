package com.getbridge.homework.web.it

import com.getbridge.homework.data.repository.DiscussionRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus

class DeleteDiscussionByIdTest : IntegrationTestBase() {

    @Autowired
    private lateinit var discussionRepository: DiscussionRepository

    @Test
    fun `test delete discussion by id should return not found when discussion is not found with id`() {
        val actual = performDelete("/discussion/10")

        assertEquals(HttpStatus.NOT_FOUND.value(), actual.response.status)
    }

    @Test
    fun `test delete discussion by id should delete discussion when discussion exist`() {
        val actual = performDelete("/discussion/2")

        assertEquals(HttpStatus.OK.value(), actual.response.status)
        assertTrue(discussionRepository.findById(2L).isEmpty)
    }
}