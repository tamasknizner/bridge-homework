package com.getbridge.homework.web.it

import com.fasterxml.jackson.databind.ObjectMapper
import com.getbridge.homework.web.domain.AUTH_USER_HEADER_NAME
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.core.io.ClassPathResource
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.util.StreamUtils
import java.nio.charset.StandardCharsets

private const val AUTH_USER_ID = "1"

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class IntegrationTestBase {

    @Autowired
    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var objectMapper: ObjectMapper

    fun performGet(url: String) = mockMvc.perform(
        get(url)
            .header(AUTH_USER_HEADER_NAME, AUTH_USER_ID)
    ).andReturn()

    fun performPost(url: String) = mockMvc.perform(
        post(url)
            .contentType("application/json")
            .header(AUTH_USER_HEADER_NAME, AUTH_USER_ID)
    ).andReturn()

    fun performPost(url: String, body: Any) = mockMvc.perform(
        post(url)
            .contentType("application/json")
            .header(AUTH_USER_HEADER_NAME, AUTH_USER_ID)
            .content(objectMapper.writeValueAsString(body))
    ).andReturn()

    fun performPut(url: String, body: Any) = mockMvc.perform(
        put(url)
            .contentType("application/json")
            .header(AUTH_USER_HEADER_NAME, AUTH_USER_ID)
            .content(objectMapper.writeValueAsString(body))
    ).andReturn()

    fun performDelete(url: String) = mockMvc.perform(
        delete(url)
            .header(AUTH_USER_HEADER_NAME, AUTH_USER_ID)
    ).andReturn()

    fun expected(fileName: String): String {
        return StreamUtils.copyToString(ClassPathResource("expected/$fileName.json").inputStream, StandardCharsets.UTF_8)
    }

}