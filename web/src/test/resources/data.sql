INSERT INTO users (id, name)
VALUES (1, 'Steve'),
       (2, 'Joe'),
       (3, 'Kathy');

INSERT INTO location (id, name)
VALUES (1, 'Teams'),
       (2, 'Zoom');

INSERT INTO discussion (id, title, description, closed, location_id, planned_date, created_date, last_modified_date)
VALUES (1, 'Steve and Kathy on Teams', 'Hi Kathy, please meet me on Teams', false, 1, '2022-02-10T10:00:00', '2022-02-08T9:00:00', '2022-02-08T9:00:00'),
       (2, 'Steve and Joe on Zoom', 'Hi Joe, please meet me on Zoom', false, 2, '2022-02-11T11:00:00', '2022-02-09T10:00:00', '2022-02-09T10:00:00'),
       (3, 'Discussion for update', 'This discussion will be updated', false, 1, '2022-02-12T12:00:00', '2022-02-09T10:00:00', '2022-02-09T10:00:00'),
       (15, 'Discussion to close', 'This discussion will be closed', false, 1, '2022-02-12T12:00:00', '2022-02-09T10:00:00', '2022-02-09T10:00:00'),
       (16, 'Closed discussion', 'This discussion is closed', true, 1, '2022-02-12T12:00:00', '2022-02-09T10:00:00', '2022-02-09T10:00:00');

INSERT INTO discussion_participants (discussion_entity_id, participants_id)
VALUES (1, 1),
       (1, 3),
       (2, 1),
       (2, 2),
       (3, 1),
       (3, 2),
       (15, 1),
       (15, 2),
       (16, 1),
       (16, 2);