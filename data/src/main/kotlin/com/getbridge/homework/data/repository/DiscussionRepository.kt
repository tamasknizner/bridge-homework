package com.getbridge.homework.data.repository

import com.getbridge.homework.data.entity.DiscussionEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface DiscussionRepository : JpaRepository<DiscussionEntity, Long> {

    fun findAllByClosedIs(closed: Boolean): List<DiscussionEntity>

}