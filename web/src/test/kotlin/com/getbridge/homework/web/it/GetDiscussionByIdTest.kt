package com.getbridge.homework.web.it

import com.getbridge.homework.web.domain.AUTH_USER_HEADER_NAME
import net.javacrumbs.jsonunit.assertj.assertThatJson
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders

class GetDiscussionByIdTest : IntegrationTestBase() {

    @Test
    fun `test get discussion by id should return discussion when found`() {

        val actual = performGet("/discussion/1")

        assertEquals(HttpStatus.OK.value(), actual.response.status)
        assertThatJson(actual.response.contentAsString).isEqualTo(expected("get-discussion-by-id"))
    }

    @Test
    fun `test get discussion by id should return not found when discussion is not found`() {

        val actual = performGet("/discussion/10")

        assertEquals(HttpStatus.NOT_FOUND.value(), actual.response.status)
    }

    @Test
    fun `test get discussion by id should return forbidden when user is not owner`() {

        val actual = mockMvc.perform(
            MockMvcRequestBuilders.get("/discussion/1")
                .header(AUTH_USER_HEADER_NAME, "5")
        ).andReturn()

        assertEquals(HttpStatus.FORBIDDEN.value(), actual.response.status)
    }
}