package com.getbridge.homework.web.controller

import com.getbridge.homework.service.DiscussionService
import com.getbridge.homework.web.domain.AUTH_USER_HEADER_NAME
import com.getbridge.homework.web.domain.DiscussionRequest
import com.getbridge.homework.web.domain.toDomain
import com.getbridge.homework.web.domain.toView
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class DiscussionRestController(
    private val validatingDiscussionService: DiscussionService
) {

    @PostMapping("/discussion")
    fun saveDiscussion(@RequestBody @Valid request: DiscussionRequest) =
        ResponseEntity.ok(validatingDiscussionService.saveDiscussion(request.toDomain()).toView())

    @PutMapping("/discussion/{id}")
    fun updateDiscussion(@PathVariable id: Long, @RequestBody @Valid request: DiscussionRequest, @RequestHeader(AUTH_USER_HEADER_NAME) userId: Long) =
        ResponseEntity.ok(validatingDiscussionService.updateDiscussion(id, request.toDomain(), userId).toView())

    @GetMapping("/discussion/{id}")
    fun getDiscussionById(@PathVariable id: Long, @RequestHeader(AUTH_USER_HEADER_NAME) userId: Long) =
        ResponseEntity.ok(validatingDiscussionService.findDiscussionById(id, userId).toView())

    @GetMapping("/discussions")
    fun getAllDiscussions() = ResponseEntity.ok(validatingDiscussionService.findAllDiscussions().map { discussion -> discussion.toView() })

    @DeleteMapping("/discussion/{id}")
    fun deleteDiscussionById(@PathVariable id: Long, @RequestHeader(AUTH_USER_HEADER_NAME) userId: Long): ResponseEntity<Unit> {
        validatingDiscussionService.deleteDiscussionById(id, userId)
        return ResponseEntity.ok().build()
    }

    @PostMapping("/discussion/{id}/close")
    fun closeDiscussion(@PathVariable id: Long, @RequestHeader(AUTH_USER_HEADER_NAME) userId: Long): ResponseEntity<Unit> {
        validatingDiscussionService.closeDiscussion(id, userId)
        return ResponseEntity.ok().build()
    }

    @GetMapping("/discussions/search")
    fun searchDiscussions(@RequestParam closed: Boolean = false) =
        ResponseEntity.ok(validatingDiscussionService.searchDiscussions(closed).map { discussion -> discussion.toView() })
}