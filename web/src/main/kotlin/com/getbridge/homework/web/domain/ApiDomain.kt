package com.getbridge.homework.web.domain

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDateTime
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Positive
import javax.validation.constraints.Size

data class DiscussionRequest(
    @field:NotBlank
    val title: String,
    @field:Size(min = 2, max = 2)
    val participants: List<Long>,
    @field:Positive
    val location: Long,
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", shape = JsonFormat.Shape.STRING)
    val plannedDate: LocalDateTime,
    @field:NotBlank
    val description: String
)

data class UserView(
    val id: Long,
    val name: String
)

data class LocationView(
    val id: Long,
    val name: String
)

data class DiscussionView(
    val id: Long,
    val title: String,
    val participants: List<UserView>,
    val location: LocationView,
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    val plannedDate: LocalDateTime,
    val description: String,
    val closed: Boolean,
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    val createdDate: LocalDateTime,
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    val lastModifiedDate: LocalDateTime
)