package com.getbridge.homework.data.dao

import com.getbridge.homework.data.repository.UserRepository
import org.springframework.stereotype.Component

@Component
class UserDao(
    private val userRepository: UserRepository
) {
    fun findById(id: Long) = userRepository.findById(id)
}