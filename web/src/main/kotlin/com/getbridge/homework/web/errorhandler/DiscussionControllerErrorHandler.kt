package com.getbridge.homework.web.errorhandler

import com.getbridge.homework.service.domain.DiscussionReadOnlyException
import com.getbridge.homework.service.domain.EntityNotFoundException
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

private val LOGGER = LoggerFactory.getLogger(DiscussionControllerErrorHandler::class.java)

@ControllerAdvice
class DiscussionControllerErrorHandler {

    @ExceptionHandler(value = [MethodArgumentNotValidException::class, HttpMessageNotReadableException::class])
    fun handleValidationError(exception: Exception) : ResponseEntity<JvmType.Object> {
        LOGGER.error("Validation error while processing request: ${exception.message}")
        return ResponseEntity.badRequest().build()
    }

    @ExceptionHandler(value = [EntityNotFoundException::class, DiscussionReadOnlyException::class])
    fun handleBusinessError(exception: Exception) : ResponseEntity<JvmType.Object> {
        LOGGER.error("Business error while processing request: ${exception.message}")
        return ResponseEntity.badRequest().build()
    }
}