package com.getbridge.homework.data.dao

import com.getbridge.homework.data.entity.DiscussionEntity
import com.getbridge.homework.data.repository.DiscussionRepository
import org.springframework.stereotype.Component

@Component
class DiscussionDao(
    private val discussionRepository: DiscussionRepository
) {
    fun save(entity: DiscussionEntity) = discussionRepository.save(entity)

    fun findById(id: Long) = discussionRepository.findById(id)

    fun deleteById(id: Long) = discussionRepository.deleteById(id)

    fun findAll(): List<DiscussionEntity> = discussionRepository.findAll()

    fun findAllByClosed(closed: Boolean) = discussionRepository.findAllByClosedIs(closed)
}