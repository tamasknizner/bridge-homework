package com.getbridge.homework.web.domain

import com.getbridge.homework.service.domain.Discussion
import com.getbridge.homework.service.domain.Location
import com.getbridge.homework.service.domain.User
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.time.Month

private const val ID = 1L
private const val NAME = "name"
private const val TITLE = "title"
private const val DESCRIPTION = "description"
private val DATE = LocalDateTime.of(2022, Month.FEBRUARY, 4, 15, 0)

private val USER = User(ID)
private val NAMED_USER = User(ID, NAME)
private val USER_VIEW = UserView(ID, NAME)

private val LOCATION = Location(ID)
private val NAMED_LOCATION = Location(ID, NAME)
private val LOCATION_VIEW = LocationView(ID, NAME)

private val DISCUSSION = Discussion(
    id = ID,
    title = TITLE,
    participants = listOf(NAMED_USER),
    location = NAMED_LOCATION,
    plannedDate = DATE,
    description = DESCRIPTION,
    closed = false,
    createdDate = DATE,
    lastModifiedDate = DATE
)

private val NEW_DISCUSSION = Discussion(
    title = TITLE,
    participants = listOf(USER),
    location = LOCATION,
    plannedDate = DATE,
    description = DESCRIPTION,
    closed = false
)

private val DISCUSSION_VIEW = DiscussionView(
    id = ID,
    title = TITLE,
    participants = listOf(USER_VIEW),
    location = LOCATION_VIEW,
    plannedDate = DATE,
    description = DESCRIPTION,
    closed = false,
    createdDate = DATE,
    lastModifiedDate = DATE
)

private val DISCUSSION_REQUEST = DiscussionRequest(
    title = TITLE,
    participants = listOf(ID),
    location = ID,
    plannedDate = DATE,
    description = DESCRIPTION
)

internal class ApiExtensionsTest {

    @Test
    fun `test user to view should convert domain object to view`() {

        val actual = NAMED_USER.toView()

        assertEquals(USER_VIEW, actual)
    }

    @Test
    fun `test location to view should convert domain object to view`() {

        val actual = NAMED_LOCATION.toView()

        assertEquals(LOCATION_VIEW, actual)
    }

    @Test
    fun `test discussion to view should convert domain object to view`() {

        val actual = DISCUSSION.toView()

        assertEquals(DISCUSSION_VIEW, actual)
    }

    @Test
    fun `test discussion request to domain should convert request object to domain object`() {

        val actual = DISCUSSION_REQUEST.toDomain()

        assertEquals(NEW_DISCUSSION, actual)
    }
}