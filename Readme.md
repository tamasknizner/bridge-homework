# Homework

## How to run and build the application?
To avoid any unpleasant experience, it is recommended to use the included maven wrapper for maven executions.

### Build
This builds the application and runs the unit and integration tests. The application uses Java 11.

``./mvnw clean install``

### Run
By default, the application uses an H2 in-memory database in order to be convenient for testing.

Running the application:

``./mvnw spring-boot:run -pl web``

There is an option to preload some data into the database with the following command by setting the `database.init` flag to true in the [application.yml](web/src/main/resources/application.yml) file.

The H2 console is available at `http://localhost:8080/h2` and can be accessed with the following username / password: `localuser / pwd`

### Run with proper database
The [docker-compose.yml](docker-compose.yml) contains a PostgreSQL database image that could be used as database when the application is started with the appropriate profile.

Disclaimer: this has not been tested thoroughly

Start the database container:

``docker-compose up``

Start the application with `postgres` profile:

``./mvnw spring-boot:run -pl web -Dapp.profiles=postgres``

Adminer is available at `http://localhost:8081/` and can be accessed with the following username / password: `postgres / Secret123`

## Application structure

The application is a standard 3-tier application with web, service and data modules.

### Web
This module contains the controllers and view layer. Also, the integration test could be found in [this](web/src/test/kotlin/com/getbridge/homework/web/it) package.

### Service
This module contains most of the business logic and some of the more extensive validation logic.

### Data
This module contains the entities and repositories.

## Suggested improvements
As I had limited time, I was not able to implement a few things properly, so here is a list of possible improvements:

- Response status codes could be changed to be more restful
- Upon creating a discussion I had to add a little hack to make the application return the nested entities, this could definitely be improved
- Auth user id handling could be extended to find-all-ish endpoints to filter those discussions which does not have the auth user as participant
  - Alternatively the auth user id handling could be solved with a servlet filter that stores the id in a request scoped bean or thread local based storage (not recommended for nio solutions tho),which could easily be injected to the required places
- The integration tests are using a common database that is initialized when the application starts. Because of this I was not able to properly validate all responses as thoroughly as I would do normally