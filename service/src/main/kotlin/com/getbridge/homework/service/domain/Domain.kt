package com.getbridge.homework.service.domain

import java.time.LocalDateTime

data class User(
    val id: Long,
    val name: String = ""
)

data class Location(
    val id: Long,
    val name: String = ""
)

data class Discussion(
    val id: Long? = null,
    val title: String,
    val participants: List<User>,
    val location: Location,
    val plannedDate: LocalDateTime,
    val description: String,
    val closed: Boolean = false,
    val createdDate: LocalDateTime? = null,
    val lastModifiedDate: LocalDateTime? = null
)

class EntityNotFoundException(message: String) : RuntimeException(message)

class DiscussionReadOnlyException(message: String) : RuntimeException(message)
