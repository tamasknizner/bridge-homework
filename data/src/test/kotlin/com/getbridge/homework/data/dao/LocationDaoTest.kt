package com.getbridge.homework.data.dao

import com.getbridge.homework.data.entity.LocationEntity
import com.getbridge.homework.data.repository.LocationRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension
import java.util.Optional

private const val ID = 1L
private val LOCATION = LocationEntity(ID, "Teams")

@ExtendWith(MockitoExtension::class)
internal class LocationDaoTest {

    @Mock
    lateinit var locationRepository: LocationRepository

    @InjectMocks
    lateinit var underTest: LocationDao

    @Test
    fun `test find location by id should return location when called with id that exist`() {
        val expected = Optional.of(LOCATION)
        `when`(locationRepository.findById(ID)).thenReturn(expected)

        val actual = underTest.findById(ID)

        assertEquals(expected, actual)
    }
}