package com.getbridge.homework.web

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan(basePackages = ["com.getbridge.homework"])
class HomeworkApplication

fun main(args: Array<String>) {
    runApplication<HomeworkApplication>(*args)
}
