package com.getbridge.homework.service

import com.getbridge.homework.data.dao.DiscussionDao
import com.getbridge.homework.data.dao.LocationDao
import com.getbridge.homework.data.dao.UserDao
import com.getbridge.homework.data.entity.DiscussionEntity
import com.getbridge.homework.service.domain.Discussion
import com.getbridge.homework.service.domain.DiscussionReadOnlyException
import com.getbridge.homework.service.domain.toClosed
import com.getbridge.homework.service.domain.toDomain
import com.getbridge.homework.service.domain.toEntity
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class DefaultDiscussionService(
    private val discussionDao: DiscussionDao,
    private val userDao: UserDao,
    private val locationDao: LocationDao
) : DiscussionService {

    override fun saveDiscussion(discussion: Discussion) = discussionDao.save(
        DiscussionEntity(
            id = discussion.id,
            title = discussion.title,
            description = discussion.description,
            // I'm honestly sorry about this ¯\_(ツ)_/¯
            participants = discussion.participants.map { it.id }.map { userDao.findById(it) }.map { optional -> optional.get() }.toList(),
            location = locationDao.findById(discussion.location.id).get(),
            closed = false,
            plannedDate = discussion.plannedDate,
            createdDate = discussion.createdDate,
            lastModifiedDate = discussion.lastModifiedDate
        )
    ).toDomain()

    override fun updateDiscussion(discussionId: Long, discussion: Discussion, userId: Long): Discussion {
        return with(findDiscussionById(discussionId, userId)) {
            if (this.closed) {
                throw DiscussionReadOnlyException("Discussion with id: ${this.id} is closed and cannot be modified.")
            } else {
                save(updateDiscussion(discussion, this)).toDomain()
            }
        }
    }

    override fun findDiscussionById(id: Long, userId: Long): Discussion {
        val discussion = discussionDao.findById(id).map { it.toDomain() }.orElseThrow { createException(id) }
        validateAccess(discussion, userId)
        return discussion
    }

    override fun deleteDiscussionById(id: Long, userId: Long) {
        findDiscussionById(id, userId)
        discussionDao.deleteById(id)
    }

    override fun findAllDiscussions() = discussionDao.findAll().map { entity -> entity.toDomain() }

    override fun closeDiscussion(id: Long, userId: Long) {
        save(findDiscussionById(id, userId).toClosed())
    }

    override fun searchDiscussions(closed: Boolean): List<Discussion> = discussionDao.findAllByClosed(closed).map { entity -> entity.toDomain() }

    private fun save(discussion: Discussion) = discussionDao.save(discussion.toEntity())

    private fun createException(id: Long) = ResponseStatusException(HttpStatus.NOT_FOUND, "Discussion not found with id $id")

    private fun validateAccess(discussion: Discussion, userId: Long) {
        if (userHasNoAccessToDiscussion(discussion, userId)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "User with id $userId has no access to discussion with id ${discussion.id}")
        }
    }

    private fun userHasNoAccessToDiscussion(discussion: Discussion, userId: Long) = !discussion.participants.map { it.id }.contains(userId)

    private fun updateDiscussion(updatedDiscussion: Discussion, existingDiscussion: Discussion): Discussion {
        return Discussion(
            id = existingDiscussion.id,
            title = updatedDiscussion.title,
            description = updatedDiscussion.description,
            participants = updatedDiscussion.participants,
            location = updatedDiscussion.location,
            closed = updatedDiscussion.closed,
            plannedDate = updatedDiscussion.plannedDate,
            createdDate = existingDiscussion.createdDate,
            lastModifiedDate = existingDiscussion.lastModifiedDate
        )
    }
}