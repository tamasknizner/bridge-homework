package com.getbridge.homework.data.config

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@Configuration
@EnableJpaRepositories(basePackages = ["com.getbridge.homework.data.repository"])
@EntityScan(basePackages = ["com.getbridge.homework.data.entity"])
@EnableJpaAuditing
class JpaConfig