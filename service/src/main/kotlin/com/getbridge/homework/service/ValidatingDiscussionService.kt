package com.getbridge.homework.service

import com.getbridge.homework.data.dao.LocationDao
import com.getbridge.homework.data.dao.UserDao
import com.getbridge.homework.service.domain.Discussion
import com.getbridge.homework.service.domain.EntityNotFoundException
import org.springframework.stereotype.Service

@Service
class ValidatingDiscussionService(
    private val defaultDiscussionService: DiscussionService,
    private val userDao: UserDao,
    private val locationDao: LocationDao
) : DiscussionService by defaultDiscussionService {

    override fun saveDiscussion(discussion: Discussion): Discussion {
        validateNewDiscussion(discussion)
        return defaultDiscussionService.saveDiscussion(discussion)
    }

    override fun updateDiscussion(discussionId: Long, discussion: Discussion, userId: Long): Discussion {
        validateNewDiscussion(discussion)
        return defaultDiscussionService.updateDiscussion(discussionId, discussion, userId)
    }

    private fun validateNewDiscussion(discussion: Discussion) {
        validateLocationExistence(discussion.location.id)
        discussion.participants.map { it.id }.forEach { validateUserExistence(it) }
    }

    private fun validateUserExistence(id: Long) = userDao.findById(id).orElseThrow { EntityNotFoundException("User not found with id: $id") }

    private fun validateLocationExistence(id: Long) = locationDao.findById(id).orElseThrow { EntityNotFoundException("Location not found with id: $id") }

}