package com.getbridge.homework.data.entity

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "discussion")
@EntityListeners(AuditingEntityListener::class)
class DiscussionEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,
    val title: String,
    @ManyToMany
    val participants: List<UserEntity>,
    @ManyToOne
    val location: LocationEntity,
    val plannedDate: LocalDateTime,
    val description: String,
    val closed: Boolean,
    @CreatedDate
    var createdDate: LocalDateTime? = null,
    @LastModifiedDate
    var lastModifiedDate: LocalDateTime? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DiscussionEntity

        if (id != other.id) return false
        if (title != other.title) return false
        if (participants != other.participants) return false
        if (location != other.location) return false
        if (plannedDate != other.plannedDate) return false
        if (description != other.description) return false
        if (closed != other.closed) return false
        if (createdDate != other.createdDate) return false
        if (lastModifiedDate != other.lastModifiedDate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + participants.hashCode()
        result = 31 * result + location.hashCode()
        result = 31 * result + plannedDate.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + closed.hashCode()
        result = 31 * result + createdDate.hashCode()
        result = 31 * result + lastModifiedDate.hashCode()
        return result
    }
}