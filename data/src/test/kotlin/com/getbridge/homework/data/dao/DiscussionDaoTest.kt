package com.getbridge.homework.data.dao

import com.getbridge.homework.data.entity.DiscussionEntity
import com.getbridge.homework.data.entity.LocationEntity
import com.getbridge.homework.data.repository.DiscussionRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.doNothing
import org.mockito.Mockito.verify
import org.mockito.junit.jupiter.MockitoExtension
import java.time.LocalDateTime
import java.time.Month
import java.util.Optional

private const val TITLE = "title"
private const val DESCRIPTION = "description"
private const val ID = 1L
private val DATE = LocalDateTime.of(2022, Month.FEBRUARY, 4, 15, 0)
private val LOCATION = LocationEntity(ID, "Teams")
private val DISCUSSION_ENTITY = DiscussionEntity(
    id = ID,
    title = TITLE,
    description = DESCRIPTION,
    participants = listOf(),
    location = LOCATION,
    closed = false,
    plannedDate = DATE,
    createdDate = DATE,
    lastModifiedDate = DATE
)

@ExtendWith(MockitoExtension::class)
internal class DiscussionDaoTest {

    @Mock
    lateinit var discussionRepository: DiscussionRepository

    @InjectMocks
    lateinit var underTest: DiscussionDao

    @Test
    fun `test save discussion entity should save the new entity and return it`() {
        val newEntity = DiscussionEntity(
            title = TITLE,
            description = DESCRIPTION,
            participants = listOf(),
            location = LOCATION,
            closed = false,
            plannedDate = DATE
        )
        `when`(discussionRepository.save(newEntity)).thenReturn(DISCUSSION_ENTITY)

        val actual = underTest.save(newEntity)

        assertEquals(DISCUSSION_ENTITY, actual)
    }

    @Test
    fun `test find discussion by id should return discussion when called with id that exist`() {
        val expected = Optional.of(DISCUSSION_ENTITY)
        `when`(discussionRepository.findById(ID)).thenReturn(expected)

        val actual = underTest.findById(ID)

        assertEquals(expected, actual)
    }

    @Test
    fun `test delete discussion by id should call repository and delete entity when called`() {
        doNothing().`when`(discussionRepository).deleteById(ID)

        underTest.deleteById(ID)

        verify(discussionRepository).deleteById(ID)
    }

    @Test
    fun `test find all should return all discussion entities when called`() {
        val discussions = listOf(DISCUSSION_ENTITY)
        `when`(discussionRepository.findAll()).thenReturn(discussions)

        val actual = underTest.findAll()

        assertEquals(discussions, actual)
    }

    @Test
    fun `test find all by closed should return all discussions with the appropriate closed state when called`() {
        val discussions = listOf(DISCUSSION_ENTITY)
        `when`(discussionRepository.findAllByClosedIs(false)).thenReturn(discussions)

        val actual = underTest.findAllByClosed(false)

        assertEquals(discussions, actual)
    }

}