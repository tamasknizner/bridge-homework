package com.getbridge.homework.web.domain

import com.getbridge.homework.service.domain.Discussion
import com.getbridge.homework.service.domain.Location
import com.getbridge.homework.service.domain.User
import java.time.LocalDateTime

fun User.toView() = UserView(id, name)

fun Location.toView() = LocationView(id, name)

fun Discussion.toView() = DiscussionView(
    id = id ?: 0L,
    title = title,
    participants = participants.map { user -> user.toView() },
    location = location.toView(),
    plannedDate = plannedDate,
    description = description,
    closed = closed,
    createdDate = createdDate ?: LocalDateTime.now(),
    lastModifiedDate = lastModifiedDate ?: LocalDateTime.now()
)

fun DiscussionRequest.toDomain() = Discussion(
    title = title,
    participants = participants.map { userId -> User(userId) },
    location = Location(location),
    plannedDate = plannedDate,
    description = description
)
